$(document).ready(function() {
  //--------------------- WESTERN SEARCH ---------------------//
  var widgetState = 0;
  $(".western-search button").click(function() {
    if (widgetState === 0) {
      $(".western-search-widget").show(1);
      $(".western-search-widget").animate({right:"44px", opacity:"1"},500);
      widgetState = 1;
    } else {
      $(".western-search-widget").animate({right:"-300px", opacity:"0"},500);
      $(".western-search-widget").hide(1);
      widgetState = 0;
    }
  });

  //-------------------- WESTERN MENUS ----------------------//
  //Quick Links Mobile Menu
  var quickLinksState = 0;
  $(".toggle-quick-links").click(function() {
    if (quickLinksState === 0) {
      $(this).parent().children("ul").animate({right:"44px"},750);
      quickLinksState = 1;
    } else {
      $(this).parent().children("ul").animate({right:"-250px"},750);
      quickLinksState = 0;
    }
  });

  //Mobile Menu
  var mobileState;
  $(".mobile-main-nav").click(function() {
    $(".menu-bar").slideToggle(300);

    if (mobileState === 0) {
      $(this).parent().children("ul").animate({right:"-250px"},750);
      $(".expand").animate({opacity:"0"},100);
      mobileState = 1;
      $(this).html('Open Mobile Navigation');
    } else {
      $(this).parent().children("ul").animate({right:"44px"},750);
      $(".expand").animate({opacity:"1"},100);
      mobileState = 0;
      $(this).html('Close Mobile Navigation');
    }
  });

  //Accordion menu
  $(".expand").click(function() {
    var buttonState = $(this).text();

    $(this).parent().children("ul").slideToggle(300);
    $(this).text(
      buttonState == "+" ? "⌃" : "+"
    );
  });

  // Current page in navigation
  $('.active > a').attr('aria-current', 'page');
});
